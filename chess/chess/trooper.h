#pragma once
#include "board.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
using std::string;


class trooper
{
public:
	virtual int isValidMove(string str, board* board) = 0;
	void move(string str, board* board);
};

