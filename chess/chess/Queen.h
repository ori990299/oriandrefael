#pragma once
#include "board.h"
#include "trooper.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
using std::string;

class Queen:public trooper
{
public:
	Queen();
	virtual int isValidMove(string str, board* board) override;
};

