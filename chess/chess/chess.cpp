#include <iostream>
#include "board.h"
#include "Rook.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
//RNBKQBNRPPPPPPPP################################pppppppprnnkqnbr0

#define DEAFULT_CHESS_BOARD "RNBKQBNR################################################rnnkqnbr0"
#define NUM_SQUARES 8 
#define A_CHAR 97
#define EIGHT_CHAR 56
#define X1_PLACE 0
#define X2_PLACE 2
#define Y1_PLACE 1
#define Y2_PLACE 3
#define ROOK_CHAR_WHITE 'R'
#define ROOK_CHAR_BLACK 'r'
#define BISHOP_CHAR_BLACK 'b'
#define BISHOP_CHAR_WHITE 'B'
#define KING_CHAR_BLACK 'k'
#define KING_CHAR_WHITE 'K'
#define KNIGHT_CHAR_BLACK 'n'
#define KNIGHT_CHAR_WHITE 'N'
#define PAWN_CHAR_BLACK 'p'
#define PAWN_CHAR_WHITE 'P'
#define QUEEN_CHAR_BLACK 'q'
#define QUEEN_CHAR_WHITE 'Q'
#define EMPTY_PLACE '#'
#define ROOK 1
#define BISHOP 2
#define KNIGHT 4
#define PAWN 5
#define QUEEN 6
#define EMPTY -1
#define KING 3
#define ZERO_CHAR 48
#define ERROR_1 2
#define ERROR_6 7
#define ERROR_4 5
#define ERROR_5 6
#define ERROR_2 3
//
int getTrooper(string str, board* board);
void move(int retValue, board* b, string str, King* king, Rook* r, Bishop* bish, Knight* k, Pawn* p, Queen* q, char board[8][8]);
bool isBlackKingThreatened(board* b, King* king, Rook* r, Bishop* bish, Knight* k, Pawn* p, Queen* q);
bool isWhiteKingThreatened(board* b, King* king, Rook* r, Bishop* bish, Knight* k, Pawn* p, Queen* q);
//
using namespace std;

int main()
{
	srand(time_t(NULL));
	char tempB[8][8];
	board b = board(DEAFULT_CHESS_BOARD);
	Rook r = Rook();
	Bishop bish = Bishop();
	King k = King();
	Pawn p = Pawn();
	Queen q;
	Knight n = Knight();
	string newPlace;
	int retValue = 0;
	int trooper = 0;


	while (true)
	{
		b.printBoard();
		cin >> newPlace;
		trooper = getTrooper(newPlace, &b);
		switch (trooper)
		{
		case ROOK:
			retValue = r.isValidMove(newPlace, &b);
			b.getBoard(tempB);
			move(retValue, &b, newPlace, &k,&r,&bish,&n,&p,&q,tempB);
			break;
		case BISHOP:
			retValue = bish.isValidMove(newPlace, &b);
			b.getBoard(tempB);
			move(retValue, &b, newPlace, &k, &r, &bish, &n, &p, &q, tempB);
			break;
		case KING:
			retValue = k.isValidMove(newPlace, &b);
			b.getBoard(tempB);
			move(retValue, &b, newPlace, &k, &r, &bish, &n, &p, &q, tempB);
			break;
		case KNIGHT:
			retValue = n.isValidMove(newPlace, &b);
			b.getBoard(tempB);
			move(retValue, &b, newPlace, &k, &r, &bish, &n, &p, &q, tempB);
			break;
		case PAWN:
			retValue = p.isValidMove(newPlace, &b);
			b.getBoard(tempB);
			move(retValue, &b, newPlace, &k, &r, &bish, &n, &p, &q, tempB);
			break;
		case QUEEN:
			retValue = q.isValidMove(newPlace, &b);
			b.getBoard(tempB);
			move(retValue, &b, newPlace, &k, &r, &bish, &n, &p, &q, tempB);
			break;
		case EMPTY:
			cout << "2\n\n";
		}
	}
}

/*
this fuhnction returns which trooper is moving now!
input:the string that represents from which place to another place, and a pointer to the board!
output:which trooper is playing now
*/
int getTrooper(string str, board* board)
{
	int x = int(str[X1_PLACE]) - A_CHAR;//source x
	int y = (int(str[Y1_PLACE]) - EIGHT_CHAR) * -1; // source y
	char c = board->getPlace(x, y);
	switch (c)
	{
	case ROOK_CHAR_BLACK:
		return ROOK;
		break;
	case ROOK_CHAR_WHITE:
		return ROOK;
		break;
	case BISHOP_CHAR_BLACK:
		return BISHOP;
		break;
	case BISHOP_CHAR_WHITE:
		return BISHOP;
		break;
	case KING_CHAR_BLACK:
		return KING;
		break;
	case KING_CHAR_WHITE:
		return KING;
		break;
	case KNIGHT_CHAR_BLACK:
		return KNIGHT;
		break;
	case KNIGHT_CHAR_WHITE:
		return KNIGHT;
		break;
	case PAWN_CHAR_BLACK:
		return PAWN;
		break;
	case PAWN_CHAR_WHITE:
		return PAWN;
		break;
	case QUEEN_CHAR_BLACK:
		return QUEEN;
		break;
	case QUEEN_CHAR_WHITE:
		return QUEEN;
		break;
	default:
		return EMPTY;
		break;
	}
}

void move(int retValue, board* b, string str, King* king, Rook* r, Bishop* bish, Knight* k, Pawn* p, Queen* q,char board[8][8])
{
	bool turn = b->getTurn();
	switch (retValue)
	{
	case 0:
		b->updateBoard(str);
		if (turn)
		{
			b->switchTurn();
			if (isWhiteKingThreatened(b, king, r, bish, k, p, q) == true)
			{
				cout << "4\n\n";
				b->setBoard(board);
			}
			b->switchTurn();
			if (isBlackKingThreatened(b, king, r, bish, k, p, q) == true)
			{
				cout << "1\n\n";
				b->switchTurn();
			}
			else
			{
				b->switchTurn();
			}
		}
		else
		{
			b->switchTurn();
			if (isBlackKingThreatened(b, king, r, bish, k, p, q) == true)
			{
				cout << "4\n\n";
				b->setBoard(board);
			}
			b->switchTurn();
			if (isWhiteKingThreatened(b, king, r, bish, k, p, q) == true)
			{
				cout << "1\n\n";
				b->switchTurn();
			}
			else
			{
				b->switchTurn();
			}
		}
		break;

	case ERROR_1:
		cout << "2\n\n";
		break;

	case ERROR_2:
		cout << "3\n\n";
		break;

	case ERROR_4:
		cout << "5\n\n";
		break;

	case ERROR_5:
		cout << "6\n\n";
		break;

	case ERROR_6:
		cout << "7\n\n";
		break;
	}
}

bool isWhiteKingThreatened(board* b, King * king, Rook * r, Bishop* bish, Knight* k, Pawn* p, Queen * q)
{
	string place = king->GetKing("white", b);
	int trooper = 0;
	bool flag = false;
	string newPlace = "**" + place;
	int retValue = 0;
	for (int i = 0; i < NUM_SQUARES && !flag; i++)
	{
		for (int j = 0; j < NUM_SQUARES && !flag; j++)
		{
			if (b->getPlace(i,j) != EMPTY_PLACE && !(isupper(b->getPlace(i, j))))
			{
				newPlace[0] = (char)(i + A_CHAR);
				newPlace[1] = (char)(((j - NUM_SQUARES) * -1) + ZERO_CHAR);
				trooper = getTrooper(newPlace, b);
				switch (trooper)
				{
				case ROOK:
					retValue = r->isValidMove(newPlace, b);
					break;
				case BISHOP:
					retValue = bish->isValidMove(newPlace, b);
					break;
				case KING:
					retValue = king->isValidMove(newPlace, b);
					break;
				case KNIGHT:
					retValue = k->isValidMove(newPlace, b);
					break;
				case PAWN:
					retValue = p->isValidMove(newPlace, b);
					break;
				case QUEEN:
					retValue = q->isValidMove(newPlace, b);
					break;
				}
				if (retValue == 0 || retValue == 1)
				{
					flag = true;
				}
			}
		}
	}
	return flag;
}

bool isBlackKingThreatened(board* b, King* king, Rook* r, Bishop* bish, Knight* k, Pawn* p, Queen* q)
{
	string place = king->GetKing("black", b);
	int trooper = 0;
	bool flag = false;
	string newPlace = "**" + place;
	int retValue = 0;
	for (int i = 0; i < NUM_SQUARES && !flag; i++)
	{
		for (int j = 0; j < NUM_SQUARES && !flag; j++)
		{
			if (b->getPlace(i, j) != EMPTY_PLACE && (isupper(b->getPlace(i, j))))
			{
				newPlace[0] = (char)(i + A_CHAR);
				newPlace[1] = (char)(((j - NUM_SQUARES) * -1) + ZERO_CHAR);
				trooper = getTrooper(newPlace, b);
				switch (trooper)
				{
				case ROOK:
					retValue = r->isValidMove(newPlace, b);
					break;
				case BISHOP:
					retValue = bish->isValidMove(newPlace, b);
					break;
				case KING:
					retValue = king->isValidMove(newPlace, b);
					break;
				case KNIGHT:
					retValue = k->isValidMove(newPlace, b);
					break;
				case PAWN:
					retValue = p->isValidMove(newPlace, b);
					break;
				case QUEEN:
					retValue = q->isValidMove(newPlace, b);
					break;
				}
				if (retValue == 0)
				{
					flag = true;
				}
			}
		}
	}
	return flag;
}