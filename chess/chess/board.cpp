#include "board.h"
#define START_CHARACTER 64
#define WHITE 0
#define BLACK !WHITE
#define EMPTY_PLACE '#'
#define NUM_SQUARES 8 
#define A_CHAR 97
#define ZERO_CHAR 48
#define EIGHT_CHAR 56
#define X1_PLACE 0
#define X2_PLACE 2
#define Y1_PLACE 1
#define Y2_PLACE 3

/*
this function(d'tor) initializes the game board by the string its given
*/
board::board(string start)
{
	this->_gameBoard = new char* [NUM_SQUARES];
	int i = 0;
	int f = 0;
	int counter = 0;
	for (i = 0; i < NUM_SQUARES; i++)
	{
		this->_gameBoard[i] = new char[NUM_SQUARES];
		for (f = 0; f < NUM_SQUARES; f++)
		{
			this->_gameBoard[i][f] = start[counter];
			counter++;
		}
	}
	this->_turn = ((int)start[START_CHARACTER] - ZERO_CHAR == WHITE);
}

/*
this functuion updates the board by the string it given by the frontend
input:the string that represent the place and the destination it should be moved
output:none
*/
void board::updateBoard(string str)
{
	int x1 = int(str[X1_PLACE]) - A_CHAR;//source x
	int x2 = int(str[X2_PLACE]) - A_CHAR;// destination x
	int y1 = (int(str[Y1_PLACE]) - EIGHT_CHAR) * -1; // source y
	int y2 = (int(str[Y2_PLACE]) - EIGHT_CHAR) * -1; // desination y
	this->_gameBoard[y2][x2] = this->_gameBoard[y1][x1];
	this->_gameBoard[y1][x1] = EMPTY_PLACE;
}


/*
this function  prints the board!
input:none
output:the board
*/
void board::printBoard()
{
	int i = 0;
	int f = 0;
	for (i = 0; i < NUM_SQUARES; i++)
	{
		for (f = 0; f < NUM_SQUARES; f++)
		{
			std::cout << this->_gameBoard[i][f];
			std::cout << " ";
		}
		std::cout << "\n";
	}
}

/*
this function(c'tor) deletes the gameboard
*/
board::~board()
{
	int i = 0;
	for (i = 0; i < NUM_SQUARES; i++)
	{
		delete this->_gameBoard[i];
	}
	delete this->_gameBoard;
}

/*
this function returns the value of a certain place in the board
*/
char board::getPlace(int x, int y)
{
	char c = this->_gameBoard[y][x];
	return c;
}

/*
this function returns who is the playing player
*/
bool board::getTurn()
{
	return this->_turn;
}

/*
this function switches the turn(from black to white)1
*/
void board::switchTurn()
{
	this->_turn = !(this->_turn);
}

void board::getBoard(char board[8][8])
{
	int i, j = 0;
	for (i = 0; i < NUM_SQUARES; i++)
	{
		for (j = 0; j < NUM_SQUARES; j++)
		{
			board[i][j] = this->_gameBoard[i][j];
		}
	}
}

void board::setBoard(char board[8][8])
{
	int i, j = 0;
	for (i = 0; i < NUM_SQUARES; i++)
	{
		this->_gameBoard[i] = new char[NUM_SQUARES];
		for (j = 0; j < NUM_SQUARES; j++)
		{
			this->_gameBoard[i][j] = board[i][j];
		}
	}
}