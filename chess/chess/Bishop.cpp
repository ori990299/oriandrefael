#include "Bishop.h"
#define EMPTY_PLACE '#'
#define NUM_SQUARES 8 
#define A_CHAR 97
#define EIGHT_CHAR 56
#define X1_PLACE 0
#define X2_PLACE 2
#define Y1_PLACE 1
#define Y2_PLACE 3
#define WHITE true
#define BLACK !WHITE
#define ERROR_1 2
#define ERROR_6 7
#define ERROR_4 5
#define ERROR_5 6
#define ERROR_2 3


Bishop::Bishop()
{

}

int Bishop::isValidMove(string str, board* b)
{
	int x1 = int(str[X1_PLACE]) - A_CHAR;//source x
	int x2 = int(str[X2_PLACE]) - A_CHAR;// destination x
	int y1 = (int(str[Y1_PLACE]) - EIGHT_CHAR) * -1; // source y
	int y2 = (int(str[Y2_PLACE]) - EIGHT_CHAR) * -1; // desination y
	bool isWhite = isupper(b->getPlace(x1, y1));
	if (!(x1 >= 0 && x1 <= 7 && x2 >= 0 && x2 <= 7 && y1 >= 0 && y1 <= 7 && y2 >= 0 && y2 <= 7))
	{
		return ERROR_4;
	}
	else if (b->getTurn() != isWhite)
	{
		return ERROR_1;
	}
	else if (x1 == x2 && y1 == y2)
	{
		return ERROR_6;
	}
	else
	{
		if (x2 > x1&& y2 < y1)//right up
		{
			if (x2 - x1 == y1 - y2)
			{
				int i;
				int j;
				for (i = x1 + 1, j = y1 - 1; i < x2 && j > y2; i++, j--)
				{
					if (b->getPlace(i, j) != EMPTY_PLACE)
					{
						return ERROR_5;
					}
				}
			}
			else
			{
				return ERROR_5;
			}
		}
		else if (x2 > x1&& y2 > y1)
		{
			if (x2 - x1 == y2 - y1)
			{
				int i;
				int j;
				for (i = x1 + 1, j = y1 + 1; i < x2 && j < y2; i++, j++)
				{
					if (b->getPlace(i, j) != EMPTY_PLACE)
					{
						return ERROR_5;
					}
				}
			}
			else
			{
				return ERROR_5;
			}
		}
		else if (x2 < x1 && y2 > y1)
		{
			if (x1 - x2 == y2 - y1)
			{
				int i;
				int j;
				for (i = x1 - 1, j = y1 + 1; i > x2&& j < y2; i--, j++)
				{
					if (b->getPlace(i, j) != EMPTY_PLACE)
					{
						return ERROR_5;
					}
				}
			}
			else
			{
				return ERROR_5;
			}
		}
		else if (x2 < x1&& y2 < y1)
		{
			if (x1 - x2 == y1 - y2)
			{
				int i;
				int j;
				for (i = x1 - 1, j = y1 - 1; i > x2&& j > y2; i--, j--)
				{
					if (b->getPlace(i, j) != EMPTY_PLACE)
					{
						return ERROR_5;
					}
				}
			}
			else
			{
				return ERROR_5;
			}
		}
		else
		{
			return ERROR_5;
		}
	}
	return 0;
}
