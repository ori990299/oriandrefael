#pragma once
#include "board.h"
#include <iostream>
#include <string>
#include "trooper.h"
using std::string;

class Rook:public trooper
{
private:
	bool _isCastling;
public:
	Rook();
	virtual int isValidMove(string str, board* board) override;
};

