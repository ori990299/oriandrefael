#include "trooper.h"


void trooper::move(string str, board* board)
{
	int ans = isValidMove(str, board);
	if (ans)
	{
		board->updateBoard(str);
	}
	else
	{
		std::cout << "error: ";
		std::cout << ans;
		std::cout << "\n";
	}
}
