#pragma once
#include <iostream>
#include <string>
using std::string;


class board
{
private:
	char** _gameBoard;
	bool _turn;
public:
	board(string start);
	~board();
	char getPlace(int x, int y);
	void updateBoard(string str);
	void printBoard();
	bool getTurn();
	void switchTurn();
	void getBoard(char board[8][8]);
	void setBoard(char board[8][8]);
};

