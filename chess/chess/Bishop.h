#pragma once
#include "board.h"
#include <iostream>
#include <string>
#include "trooper.h"
using std::string;
class Bishop :public trooper
{
public:
	Bishop();
	virtual int isValidMove(string str, board* board) override;
};

