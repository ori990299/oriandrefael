#pragma once
#include "board.h"
#include <iostream>
#include <string>
#include "trooper.h"
using std::string;

class Pawn:public trooper
{
public:
	Pawn();
	virtual int isValidMove(string str, board* board) override;
};

