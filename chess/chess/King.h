#pragma once
#include "board.h"
#include "trooper.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
using std::string;


class King:public trooper
{
private:
	bool _isCastling;
public:
	King();
	string GetKing(string king, board* b);
	virtual int isValidMove(string str, board* board) override;
};

