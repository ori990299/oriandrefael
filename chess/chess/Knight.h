#pragma once
#include "board.h"
#include <iostream>
#include <string>
#include "trooper.h"
using std::string;


class Knight:public trooper
{
public:
	Knight();
	virtual int isValidMove(string str, board* board) override;
};

